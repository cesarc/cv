$(document).ready(function() {
    $('a[href^="#"]').click(function() {
        console.log(this.id);
        console.log(this.parentNode.parentNode);
        let siblings = [];
        let sibling = this.parentNode.parentNode.firstChild;
        while (sibling) {
            if (sibling.nodeType === 1 && sibling !== this.parentNode) {
                sibling.classList.remove("active");
                siblings.push(sibling);
            }
            sibling = sibling.nextSibling;
        }

        this.parentNode.classList.add("active");
        // this.classList.add = "deactive";
        console.log(this.classList);
        console.log(siblings);
        var destino = $(this.hash); //this.hash lee el atributo href de este
        console.log("destino: " + destino.selector);
        let img_nav = document.getElementById("idLogoNav");
        if (destino.selector === '') {
            $('html, body').animate({ scrollTop: 0 }, 700); //Llega a su destino con el tiempo deseado  
            img_nav.classList.remove("animate__fadeInDownBig");
            img_nav.classList.add("animate__fadeOutDownBig");
            img_nav.style.visibility = 'hidden';
        } else {
            $('html, body').animate({ scrollTop: destino.offset().top - 95 }, 700); //Llega a su destino con el tiempo deseado
            img_nav.style.visibility = 'visible';
            img_nav.classList.remove("animate__fadeOutDownBig");
            img_nav.classList.add("animate__fadeInDownBig");
        }
        return false;

    });
});

// document.getElementById("home").addEventListener("mouseover", (event) => { displayMenu("home"); });
// document.getElementById("home-1").addEventListener("mouseover", (event) => { displayMenu("home"); });
// document.getElementById("home-2").addEventListener("mouseover", (event) => { displayMenu("home"); });
// document.getElementById("home-3").addEventListener("mouseover", (event) => { displayMenu("home"); });
// document.getElementById("home-4").addEventListener("mouseover", (event) => { displayMenu("home"); });
// // document.getElementById("call-center").addEventListener("mouseover", (event) => { displayMenu("call-center"); });
// // document.getElementById("troncal-sip").addEventListener("mouseover", (event) => { displayMenu("troncal-sip"); });
// document.getElementById("conocenos").addEventListener("mouseover", (event) => { displayMenu("conocenos"); });
// document.getElementById("contacto").addEventListener("mouseover", (event) => { displayMenu("contacto"); });
// document.getElementById("beneficios_id").addEventListener("mouseover", (event) => { displayMenu("beneficios_id"); });
// document.getElementById("about_nexfonIP_id").addEventListener("mouseover", (event) => { displayMenu("about_nexfonIP_id"); });

// function displayMenu(data) {
//     let card_app_id = document.getElementById("card_app_id");
//     let card_buzon_id = document.getElementById("card_buzon_id");
//     let card_contacto_id = document.getElementById("card_contacto_id");
//     let card_call_id = document.getElementById("card_call_id");
//     let img_home = document.getElementById("nosotors_img_id");
//     let img_nav = document.getElementById("idLogoNav");

//     let siblings = [];
//     let sibling = document.getElementById("nav_" + data).parentNode.parentNode.firstChild;

//     img_nav.classList.remove("animate__fadeOutDownBig");
//     img_nav.style.visibility = 'visible';
//     img_nav.classList.add("animate__fadeInDownBig");
//     if (data === "beneficios_id") {
//         card_app_id.classList.add("animate__backInLeft");
//         card_buzon_id.classList.add("animate__backInDown");
//         card_contacto_id.classList.add("animate__backInRight");
//         card_call_id.classList.add("animate__backInUp");
//         img_home.classList.remove("animate__backInLeft");
//     } else if (data === "about_nexfonIP_id") {
//         img_home.classList.add("animate__backInLeft");
//     } else {
//         img_home.classList.remove("animate__backInLeft");
//         card_app_id.classList.remove("animate__backInLeft");
//         card_buzon_id.classList.remove("animate__backInDown");
//         card_contacto_id.classList.remove("animate__backInRight");
//         card_call_id.classList.remove("animate__backInUp");

//         if (data === "home" || data === "home-1" || data === "home-2" || data === "home-3" || data === "home-4") {
//             img_nav.classList.remove("animate__fadeInDownBig");
//             img_nav.classList.add("animate__fadeOutDownBig");
//             img_nav.style.visibility = 'hidden';
//         }
//     }
//     while (sibling) {
//         if (sibling.nodeType === 1 && sibling !== this.parentNode) {
//             sibling.classList.remove("active");
//             siblings.push(sibling);
//         }
//         sibling = sibling.nextSibling;
//     }
//     document.getElementById("nav_" + data).parentNode.classList.add("active");
//     console.log(data);
// }


$(".paroller-about-me, [data-paroller-factor]").paroller({
    factor: 0.3,            // multiplier for scrolling speed and offset
    factorXs: 0.08,          // multiplier for scrolling speed and offset if window width is <576px
    factorSm: 0.18,          // multiplier for scrolling speed and offset if window width is <=768px
    factorMd: 0.18,          // multiplier for scrolling speed and offset if window width is <=1024px
    factorLg: 0.28,          // multiplier for scrolling speed and offset if window width is <=1200px
    type: 'foreground',     // background, foreground
    direction: 'horizontal', // vertical, horizontal
    transition: 'transform 0.1s ease' // CSS transition, added on elements where type:'foreground'
});

$('.paroller-resume').paroller({
    factorXs: 0.1,
    factorSm: 0.1,
    factorMd: -0.1,
    factorLg: -0.1,
    factorXl: -0.1,
    factor: -0.4,
    type: 'foreground',
    direction: 'horizontal'
    });

$('.paroller-portfolio').paroller({
    factor: 0.1,            // multiplier for scrolling speed and offset
    factorXs: 0.18,          // multiplier for scrolling speed and offset if window width is <576px
    factorSm: 0.28,          // multiplier for scrolling speed and offset if window width is <=768px
    factorMd: 0.28,          // multiplier for scrolling speed and offset if window width is <=1024px
    factorLg: 0.38,
    type: 'foreground',
    direction: 'horizontal'
    });

$('.paroller-contacts').paroller({
    factorXs: 0.1,
    factorSm: 0.5,
    factorMd: -0.5,
    factorLg: -0.5,
    factorXl: -0.5,
    factor: -0.9,
    type: 'foreground',
    direction: 'horizontal'
    });